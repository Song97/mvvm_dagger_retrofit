package com.example.mvvm;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import android.os.Bundle;
import android.widget.TextView;

import com.example.mvvm.MVVM.NewsViewModelFactory;
import com.example.mvvm.MVVM.ViewModels.NewsViewModel;
import com.example.mvvm.Retrofit.AllNews;
import com.example.mvvm.Retrofit.Article;

import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity {

    @Inject
    String title;


    @Inject
    NewsViewModelFactory factory;

    private NewsViewModel  newsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidInjection.inject(this);


        final TextView textView = findViewById(R.id.tvTesting);
        textView.setText(title);

        newsViewModel = ViewModelProviders.of(this, factory).get(NewsViewModel.class);

        newsViewModel.getAll().observe(this, new Observer<AllNews<Article>>() {
            @Override
            public void onChanged(AllNews<Article> articleAllNews) {
                   List<Article> lists = articleAllNews.getArticles();
                    textView.setText(articleAllNews.getArticles().toString());
            }
        });



    }
}
