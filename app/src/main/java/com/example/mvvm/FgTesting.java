package com.example.mvvm;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mvvm.MVVM.NewsViewModelFactory;
import com.example.mvvm.MVVM.ViewModels.NewsViewModel;
import com.example.mvvm.Retrofit.AllNews;
import com.example.mvvm.Retrofit.Article;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.support.DaggerFragment;

public class FgTesting extends DaggerFragment{

    private NewsViewModel viewModel;

    public FgTesting() {
        // Required empty public constructor
    }

    @Inject
    NewsViewModelFactory factory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fg_testing, container, false);
        final TextView tv = view.findViewById(R.id.fgTextView);

        AndroidInjection.inject(getActivity());

        viewModel = ViewModelProviders.of(getActivity(),factory).get(NewsViewModel.class);
        viewModel.getAll().observe(getActivity(), new Observer<AllNews<Article>>() {
            @Override
            public void onChanged(AllNews<Article> articleAllNews) {
                tv.setText(articleAllNews.getArticles().toString());
            }
        });

        return view;
    }

}
