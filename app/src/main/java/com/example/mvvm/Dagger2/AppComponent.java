package com.example.mvvm.Dagger2;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;


@Component(modules = {AndroidSupportInjectionModule.class,MyActivityModul.class, MyModuls.class})
@Singleton
public interface AppComponent extends AndroidInjector<DaggerApplication> {
    @Component.Builder
    interface Builde{
        @BindsInstance
        Builde application(Application application);
        AppComponent builders();
    }
}
