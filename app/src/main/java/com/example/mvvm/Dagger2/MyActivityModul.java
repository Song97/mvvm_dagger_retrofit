package com.example.mvvm.Dagger2;

import com.example.mvvm.FgTesting;
import com.example.mvvm.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyActivityModul {
    @ContributesAndroidInjector
    abstract MainActivity mainActivity();
    @ContributesAndroidInjector
    abstract FgTesting fgTesting();
}
